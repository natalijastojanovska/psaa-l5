package com.company;

import java.util.HashMap;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int pairs = 0;

        String key = new String();
        String value = new String();

        char temp1;

        HashMap<String, String> dictionary = new HashMap<String, String>();

        Scanner scan = new Scanner(System.in);


        System.out.println("Vnesi broj na zborovi: ");
        pairs = scan.nextInt();

        for(int i = 0; i < pairs; i++){
            System.out.println("Vnesi go " + (i+1)  +"-ot zbor: ");

            key = scan.next();
            value = scan.next();

            dictionary.put(key, value);
        }

        Scanner sc = new Scanner(System.in);
        System.out.println("Vnesi recenica: ");
        String sentence = sc.nextLine();

        String[] temp = sentence.split(" ");
        String nova = new String();


        for(int i = 0; i < temp.length; i++){
            if(temp[i].charAt(temp[i].length() - 1) == ',' || temp[i].charAt(temp[i].length() - 1) == '?' || temp[i].charAt(temp[i].length() - 1) == '!' || temp[i].charAt(temp[i].length() - 1) == '.'){
                key = temp[i].substring(0, temp[i].length() - 1);
                temp1 = temp[i].charAt(temp[i].length()-1);
            }
            else {
                key = temp[i];
                temp1 = ' ';
            }

            if(dictionary.containsKey(key) == true){
                nova += dictionary.get(key);
                nova += temp1;
            }
            else{
                nova += "?";
                nova += temp1;
            }
        }
        System.out.print(nova);

   }
}
