package com.company;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner br = new Scanner(System.in);
        String[] commands = new String[10];
        int k = 0;
        while (true) {
            commands[k] = br.nextLine();
            if (commands[k].equals("pechati")) {
                break;
            }
            k++;
        }
        br.close();
        NDG<Character> g;
        String prva[] = commands[0].split(" ");
        g = new NDG<>(Integer.parseInt(prva[1]));
        Character bukva = 'A';
        for (int i = 0; i < k + 1; i++) {
            String pom[] = commands[i].split(" ");
            if (pom[0].equals("Kreiraj")) {
                for (int j = 0; j < Integer.parseInt(pom[1]); j++) {
                    g.setInfo(j, bukva++);
                }
            }
            if (pom[0].equals("dodadiVrska")) {
                g.addEdge(Integer.parseInt(pom[1]), Integer.parseInt(pom[2]));
            }
            if (pom[0].equals("brishiVrska")) {
                g.deleteEdge(Integer.parseInt(pom[1]), Integer.parseInt(pom[2]));
            }
            if (pom[0].equals("dodadiJazol")) {
                g.addNode(bukva++);
            }
            if (pom[0].equals("brishiJazol")) {
                g.deleteNode(pom[1].charAt(0));
            }
            if (pom[0].equals("pechati")) {
                g.printMatrix();
            }
        }
    }
}
