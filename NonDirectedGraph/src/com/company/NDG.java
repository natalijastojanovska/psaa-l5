package com.company;

public class NDG <E>{

    int n;
    int matrix[][];

    E infos[];


    public NDG(int n) {
        this.n = n;
        infos = (E[]) new Object[n];
        matrix = new int[n][n];

        for (int i = 0; i < n; i++){
            for (int j = 0; j < n; j++){
                matrix[i][j] = 0;
            }
        }
    }

    void addEdge(int x, int y){
        matrix[x][y] = 1;
        matrix[y][x] = 1;
    }

    void deleteEdge(int x, int y){
        matrix[x][y] = 0;
        matrix[y][x] = 0;
    }

    E getInfo(int index) {
        return infos[index];
    }

    void setInfo(int index, E info) {
        this.infos[index] = info;
    }

    int getIndex(E info){
        for(int i = 0; i < n; i++){
            if(infos[i] == info){
                return i;
            }
        }
        return -1;
    }

    boolean neighbors(int x, int y){
        if(matrix[x][y] == 1){
            return true;
        }
        return false;
    }

    void addNode(E info){
        n++;
        E[] infosTemp = (E[]) new Object[n];
        for(int i = 0; i < n-1; i++){
            infosTemp[i] = infos[i];
        }

        int matrixTemp[][] = new int[n][n];
        for(int i = 0; i < n-1; i++){
            for(int j = 0; j < n-1; j++){
                matrixTemp[i][j] = matrix[i][j];
            }
        }

        infosTemp[n - 1] = info;
        for (int i = 0; i < n; i++) {
            matrixTemp[i][n - 1] = 0;
            matrixTemp[n - 1][i] = 0;
        }
        matrix = matrixTemp;
        infos = infosTemp;
    }

    void deleteNode(E info) {
        int index = getIndex(info);
        if (index != n - 1)
        {
            for (int i = index; i < n - 1; i++) {
                for (int j = 0; j < n; j++) {
                    matrix[j][i] = matrix[j][i + 1];
                }
            }
            for (int i = index; i < n - 1; i++) {
                for (int j = 0; j < n; j++) {
                    matrix[i][j] = matrix[i + 1][j];
                }
            }
        }
        n--;
    }

    public void printMatrix() {
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < n; j++) {
                System.out.print(matrix[i][j] + " ");
            }
            System.out.println();
        }
    }

    void dfsMatRek(int poseteni[], int start) {
        poseteni[start] = 1;
        System.out.println(infos[start]);
        for (int i = 0; i < n; i++) {
            if (poseteni[i] == 0 && matrix[i][start] > 0)
            {
                dfsMatRek(poseteni, i);
            }
        }
    }
}



